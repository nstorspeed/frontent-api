import React, {Component, useState} from 'react';
import './App.css';
import Modal from "react-bootstrap/Modal";
import 'bootstrap/dist/css/bootstrap.min.css';
import $ from 'jquery';
import Popper from 'popper.js';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import axios from 'axios';

export default class App extends Component{

  constructor (props) {
    super(props);

    this.state = {
      product: [],
      error: null,
      status: 0,
      isOpen: false,
      name_product: "",
      code : "",
      value: "",
    }
    this.createProduct = this.createProduct.bind(this);
    this.handleChange = this.handleChange.bind(this);

  }

  componentDidMount () {
    this.loadData();
  }

  loadData() {
    axios.get('https://api-product1020.herokuapp.com/promises',{ headers: {'Content-Type': 'application/json' } })
    .then(response => {
      console.log(response.data);
      return response.data;

    })
    .then(res => {
      var status = res.code_response;
      var error = res.error;
      var data = res.data;
      this.setState({
        product: data,
        error: error,
        status: status
      })
      console.log(data);
    })
  }

  createProduct(id) {
    this.closeModal();
    let data = {
     "name_product" : this.state.name_product,
     "code" : this.state.code,
     "value" : this.state.value
    }
    
    axios.post('https://api-product1020.herokuapp.com/promises',data,
      { headers: { 'Content-Type': 'application/json' } }
    )
    .then(response => {
      console.log(response)
      this.loadData();
    })
    .catch(error => {
      console.log(error);
    })
  }

  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  cleanModal() {
    this.closeModal();
  }

  closeModal() {
    this.setState({
      isOpen: false
    });
  }

  openModal() {
    this.setState({
      isOpen: true
    });
  }
  render(){
    if (this.state.status == 0) {
      return(
        <div>
          <h1>Cargando información de MongoDB..</h1>
        </div>
      )
    }
    
    const name = this.state.product[0].name_product;
    return(
      <div className="container">
        <h3>Productos Almacenados en MongoDB</h3>
        <div className="btn-group">
          <button type="button" onClick={()=>{this.openModal()}} className="btn btn-danger">Crear nuevo Producto</button>
        </div>
        <div className="col-lg-12 row d-flex justify-content-center">
          <table className="table table-striped table-bordered table-hover">
            <thead className="thead-dark">
              <tr>
                <th>Código</th>
                <th>Nombre</th>
                <th>Valor</th>
              </tr>
            </thead>
            <tbody>
                {
                  this.state.product.map(item => {
                    return (
                      <tr key={item._id}>
                        <td>{item.code}</td>
                        <td>{item.name_product}</td>
                        <td className="text-right">{item.value}</td>
                      </tr>
                    )
                  })
                }
            </tbody>
          </table>
        </div>
        <Modal show={this.state.isOpen} >
          <Modal.Header>Crear Producto</Modal.Header>
          <Modal.Body>
            <div>
              <div className="form-group">
                <label htmlFor="code">Código del producto</label>
                <input type="text" 
                      className="form-control" 
                      id="code" 
                      name="code" 
                      value={this.state.code}  
                      onChange={this.handleChange.bind(this)} 
                      placeholder="Ingrese el código del producto" />
              </div>
              <div className="form-group">
                <label htmlFor="name_product">Nombre del producto</label>
                <input type="text" 
                      className="form-control" 
                      id="name_product" 
                      name="name_product" 
                      placeholder="Ingrese el nombre del producto" 
                      value={this.state.name_product} 
                      onChange={this.handleChange.bind(this)}   />
              </div>
              <div className="form-group">
                <label htmlFor="value">Valor del producto</label>
                <input type="text" 
                      className="form-control" 
                      id="value" 
                      name="value" 
                      placeholder="Ingrese el valor del producto" 
                      value={this.state.value} 
                      onChange={this.handleChange.bind(this)}  />
              </div>
            </div>
          </Modal.Body>
          <Modal.Footer>
            <div className="btn-group">
              <button type="button" onClick={()=>{this.cleanModal()}} className="btn btn-secondary">Cancelar</button>
            </div>
            <div className="btn-group">
              <button type="button" onClick={()=>{this.createProduct()}} className="btn btn-danger">Almacenar</button>
            </div>
          </Modal.Footer>
        </Modal>
        
      </div>
    )
  }
}
